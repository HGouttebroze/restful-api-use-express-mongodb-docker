# Environnement dependencies

- Create a npm paquage:

  `yarn init`

- install TypeScript:

  `yarn add typescript`

- create a `tsconfig.ts` file to configure TypeScript:

  `touch tsconfig.ts`

- add Express CORS Mongoose (MongoDB) (using yarn)

  `yarn add express cors mongoose`

* adding their types as development dependencies.
  This will help the TypeScript computer understanding the packages.

`yarn add -D @types/node @types/express @types/mongoose @types/cors`

- Let's add some dependencies for auto-reloading the server when a file is modified and start the server concurrently (We'll be able to make changes and start the server simultaneously).
  `yarn add -D concurrently nodemon`

- We need to update the package.json file with the scripts needed to start the server and build the project.
  Here's how your package.json file should look.

{
"name": "menu-node-api",
"version": "1.0.0",
"main": "index.js",
"license": "MIT",
"dependencies": {
"cors": "^2.8.5",
"express": "^4.17.1",
"mongoose": "^6.0.11",
"nodemon": "^2.0.13",
"typescript": "^4.4.4"
},
"scripts": {
"build": "tsc",
"start": "concurrently \"tsc -w\" \"nodemon dist/js/app.js\""
},
"devDependencies": {
"@types/cors": "^2.8.12",
"@types/express": "^4.17.13",
"@types/mongoose": "^5.11.97",
"@types/node": "^16.11.1",
"concurrently": "^6.3.0"
}
}
